### Some suggested articles
- https://camdavidsonpilon.github.io/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/
- https://towardsdatascience.com/bayesian-a-b-testing-in-pymc3-54dceb87af74
- https://towardsdatascience.com/bayesian-ab-testing-part-iii-test-duration-f2305215009c
- https://towardsdatascience.com/how-to-analyse-a-b-experiments-using-bayesian-expected-loss-b959e21a77ce
- http://homepages.math.uic.edu/~rgmartin/Teaching/Stat532/532notes_bayes.pdf